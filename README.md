# GeoAnnotateWeb

Course project for “C# ASP.NET Core developer” at OTUS.RU by IORAS/SAIL/DS team. This project implements client-server web-based annotation tool for geospatial data like geophysical modeling outputs, remote sensing data , etc.
